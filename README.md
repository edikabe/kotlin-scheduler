# Kotlin scheduler 0.0.1

Simple example of a Spring app written in Kotlin which schedules logging of a "ping" message at a fixed delay.

## Build
```bash
docker build -t julglotain/kotlin-scheduler .
```

## Run
```bash
docker run -e 'DELAY=1000' julglotain/kotlin-scheduler
```

